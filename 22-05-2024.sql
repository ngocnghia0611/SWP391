USE [22-05-2024]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[id] [int] NOT NULL,
	[first_name] [nvarchar](50) NULL,
	[last_name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AnswerType]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AnswerType](
	[AnswerTypeID] [int] NOT NULL,
	[TypeQuesion] [bit] NULL,
 CONSTRAINT [PK_AnswerType] PRIMARY KEY CLUSTERED 
(
	[AnswerTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Avata]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Avata](
	[avataId] [int] NOT NULL,
	[avataIMG] [nvarchar](255) NULL,
 CONSTRAINT [PK_Avata] PRIMARY KEY CLUSTERED 
(
	[avataId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bank_Question]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bank_Question](
	[QuestionId] [int] IDENTITY(1,1) NOT NULL,
	[id] [nchar](10) NULL,
 CONSTRAINT [PK_Bank_Question] PRIMARY KEY CLUSTERED 
(
	[QuestionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Class]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Class](
	[classId] [int] IDENTITY(1,1) NOT NULL,
	[className] [nvarchar](255) NULL,
	[classTypeId] [int] NULL,
	[id] [int] NULL,
 CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED 
(
	[classId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Controller]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Controller](
	[id] [int] NULL,
	[first_name] [nvarchar](50) NULL,
	[last_name] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DificultyLevel]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DificultyLevel](
	[DificultyLevelID] [int] NOT NULL,
	[Lever] [nvarchar](50) NULL,
 CONSTRAINT [PK_DificultyLevel] PRIMARY KEY CLUSTERED 
(
	[DificultyLevelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Manager]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manager](
	[id] [int] NOT NULL,
	[first_name] [nvarchar](50) NULL,
	[last_name] [nvarchar](50) NULL,
	[phone] [nvarchar](10) NULL,
 CONSTRAINT [PK_Manager] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NumberOfQuestion]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NumberOfQuestion](
	[numberOfQuestionId] [int] NOT NULL,
	[numberOfQuestion] [int] NULL,
 CONSTRAINT [PK_NumberOfQuestion] PRIMARY KEY CLUSTERED 
(
	[numberOfQuestionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Question]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[idQuestion] [int] IDENTITY(1,1) NOT NULL,
	[QuestionText] [nvarchar](255) NULL,
	[classTypeId] [int] NULL,
	[DificultyLevelID] [int] NULL,
	[AnswerTypeID] [int] NULL,
	[CorrectAnswer] [nvarchar](10) NULL,
	[id] [int] NULL,
 CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED 
(
	[idQuestion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuestionQuiz]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuestionQuiz](
	[idQuestion] [int] NULL,
	[idQuiz] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuizId]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuizId](
	[idQuiz] [int] IDENTITY(1,1) NOT NULL,
	[nameQuiz] [nvarchar](100) NULL,
	[classId] [int] NULL,
	[status] [bit] NULL,
	[typeTestId] [int] NULL,
	[timeStart] [time](7) NULL,
	[timeEnd] [time](7) NULL,
	[quizStatusId] [int] NULL,
 CONSTRAINT [PK_QuizId] PRIMARY KEY CLUSTERED 
(
	[idQuiz] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[roleId] [int] NOT NULL,
	[roleName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StatusQuiz]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StatusQuiz](
	[quizStatusId] [int] NOT NULL,
	[status] [bit] NULL,
	[id] [int] NULL,
	[roleId] [int] NULL,
 CONSTRAINT [PK_StatusQuiz] PRIMARY KEY CLUSTERED 
(
	[quizStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[id] [int] NOT NULL,
	[first_name] [nvarchar](50) NULL,
	[last_name] [nvarchar](50) NULL,
	[phone] [nvarchar](10) NULL,
	[address] [nvarchar](50) NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Teacher]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teacher](
	[id] [int] NOT NULL,
	[first_name] [nvarchar](50) NULL,
	[last_name] [nvarchar](50) NULL,
	[phone] [nvarchar](10) NULL,
 CONSTRAINT [PK_Teacher] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Type_Of_Class]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type_Of_Class](
	[classTypeId] [int] NOT NULL,
	[typeClass] [nvarchar](255) NULL,
 CONSTRAINT [PK_Type_Of_Class] PRIMARY KEY CLUSTERED 
(
	[classTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Type_of_test]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type_of_test](
	[typeTestId] [int] IDENTITY(1,1) NOT NULL,
	[testName] [nvarchar](50) NULL,
	[numberOfQuestionId] [int] NULL,
 CONSTRAINT [PK_Type_of_test] PRIMARY KEY CLUSTERED 
(
	[typeTestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 22/05/2024 7:28:31 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[roldId] [int] NULL,
	[avataId] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Admin]  WITH CHECK ADD  CONSTRAINT [FK_Admin_User] FOREIGN KEY([id])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[Admin] CHECK CONSTRAINT [FK_Admin_User]
GO
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [FK_Class_Type_Of_Class] FOREIGN KEY([classTypeId])
REFERENCES [dbo].[Type_Of_Class] ([classTypeId])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [FK_Class_Type_Of_Class]
GO
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [FK_Class_User] FOREIGN KEY([id])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [FK_Class_User]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_AnswerType] FOREIGN KEY([AnswerTypeID])
REFERENCES [dbo].[AnswerType] ([AnswerTypeID])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_AnswerType]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_DificultyLevel] FOREIGN KEY([DificultyLevelID])
REFERENCES [dbo].[DificultyLevel] ([DificultyLevelID])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_DificultyLevel]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Type_Of_Class] FOREIGN KEY([classTypeId])
REFERENCES [dbo].[Type_Of_Class] ([classTypeId])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Type_Of_Class]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_User] FOREIGN KEY([id])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_User]
GO
ALTER TABLE [dbo].[QuestionQuiz]  WITH CHECK ADD  CONSTRAINT [FK_QuestionQuiz_Question] FOREIGN KEY([idQuestion])
REFERENCES [dbo].[Question] ([idQuestion])
GO
ALTER TABLE [dbo].[QuestionQuiz] CHECK CONSTRAINT [FK_QuestionQuiz_Question]
GO
ALTER TABLE [dbo].[QuestionQuiz]  WITH CHECK ADD  CONSTRAINT [FK_QuestionQuiz_QuizId] FOREIGN KEY([idQuiz])
REFERENCES [dbo].[QuizId] ([idQuiz])
GO
ALTER TABLE [dbo].[QuestionQuiz] CHECK CONSTRAINT [FK_QuestionQuiz_QuizId]
GO
ALTER TABLE [dbo].[QuizId]  WITH CHECK ADD  CONSTRAINT [FK_QuizId_Class] FOREIGN KEY([classId])
REFERENCES [dbo].[Class] ([classId])
GO
ALTER TABLE [dbo].[QuizId] CHECK CONSTRAINT [FK_QuizId_Class]
GO
ALTER TABLE [dbo].[QuizId]  WITH CHECK ADD  CONSTRAINT [FK_QuizId_StatusQuiz] FOREIGN KEY([quizStatusId])
REFERENCES [dbo].[StatusQuiz] ([quizStatusId])
GO
ALTER TABLE [dbo].[QuizId] CHECK CONSTRAINT [FK_QuizId_StatusQuiz]
GO
ALTER TABLE [dbo].[QuizId]  WITH CHECK ADD  CONSTRAINT [FK_QuizId_Type_of_test] FOREIGN KEY([typeTestId])
REFERENCES [dbo].[Type_of_test] ([typeTestId])
GO
ALTER TABLE [dbo].[QuizId] CHECK CONSTRAINT [FK_QuizId_Type_of_test]
GO
ALTER TABLE [dbo].[StatusQuiz]  WITH CHECK ADD  CONSTRAINT [FK_StatusQuiz_Role] FOREIGN KEY([roleId])
REFERENCES [dbo].[Role] ([roleId])
GO
ALTER TABLE [dbo].[StatusQuiz] CHECK CONSTRAINT [FK_StatusQuiz_Role]
GO
ALTER TABLE [dbo].[StatusQuiz]  WITH CHECK ADD  CONSTRAINT [FK_StatusQuiz_User] FOREIGN KEY([id])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[StatusQuiz] CHECK CONSTRAINT [FK_StatusQuiz_User]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK_Student_User] FOREIGN KEY([id])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK_Student_User]
GO
ALTER TABLE [dbo].[Type_of_test]  WITH CHECK ADD  CONSTRAINT [FK_Type_of_test_NumberOfQuestion] FOREIGN KEY([numberOfQuestionId])
REFERENCES [dbo].[NumberOfQuestion] ([numberOfQuestionId])
GO
ALTER TABLE [dbo].[Type_of_test] CHECK CONSTRAINT [FK_Type_of_test_NumberOfQuestion]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Avata] FOREIGN KEY([avataId])
REFERENCES [dbo].[Avata] ([avataId])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Avata]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Manager] FOREIGN KEY([id])
REFERENCES [dbo].[Manager] ([id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Manager]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([id])
REFERENCES [dbo].[Role] ([roleId])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Role]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Teacher] FOREIGN KEY([id])
REFERENCES [dbo].[Teacher] ([id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Teacher]
GO
