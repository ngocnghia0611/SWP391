﻿using Microsoft.VisualBasic.Devices;
using System.Net;
using System.Net.Mail;

namespace Email
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Random random = new Random();
        int otp;
        string userPassword;

        private void btnOTP_Click(object sender, EventArgs e)
        {
            try
            {
                otp = random.Next(100000, 999999);

                var fromAddress = new MailAddress("phongkhaothihotrosinhvien@gmail.com"); //mail dùng để gửi mã OTP
                var toAddress = new MailAddress(textEmail.ToString()); //mail dùng để nhận mã otp
                const string frompass = "dgio rreh nqki mxzg";
                const string subject = "OTP code đổi mật khẩu";
                string additionalContent = "Your OTP code is: ";
                string body = additionalContent + otp.ToString();

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, frompass),
                    EnableSsl = true,
                    Timeout = 200000
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
                MessageBox.Show("OTP đã gửi qua mail");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnVerifyOTP_Click(object sender, EventArgs e)
        {
            try
            {
                if (int.TryParse(txtOTP.Text.Trim(), out int enteredOtp))
                {
                    if (enteredOtp == otp)
                    {
                        MessageBox.Show("Xác minh OTP thành công!");
                        userPassword = otp.ToString();

                        // Hiển thị thông báo đăng nhập thành công
                        lblLoginStatus.Text = "Đăng nhập thành công! Mật khẩu mới của bạn là: " + userPassword;
                    }
                    else
                    {
                        MessageBox.Show("Mã OTP không đúng. Vui lòng thử lại.");
                    }
                }
                else
                {
                    MessageBox.Show("Vui lòng nhập một mã OTP hợp lệ.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

       
    }
}
