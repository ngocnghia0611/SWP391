﻿namespace Email
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            label1 = new Label();
            label2 = new Label();
            textEmail = new TextBox();
            txtOTP = new TextBox();
            btnOTP = new Button();
            btnVerifyOTP = new Button();
            imageList1 = new ImageList(components);
            button1 = new Button();
            label3 = new Label();
            lblLoginStatus = new Label();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(119, 120);
            label1.Name = "label1";
            label1.Size = new Size(74, 15);
            label1.TabIndex = 0;
            label1.Text = "Nhập Email :";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(119, 172);
            label2.Name = "label2";
            label2.Size = new Size(86, 15);
            label2.TabIndex = 1;
            label2.Text = "Nhập mã OTP :";
            // 
            // textEmail
            // 
            textEmail.Location = new Point(205, 112);
            textEmail.Name = "textEmail";
            textEmail.Size = new Size(221, 23);
            textEmail.TabIndex = 2;
            // 
            // txtOTP
            // 
            txtOTP.Location = new Point(205, 164);
            txtOTP.Name = "txtOTP";
            txtOTP.Size = new Size(100, 23);
            txtOTP.TabIndex = 3;
            // 
            // btnOTP
            // 
            btnOTP.Location = new Point(322, 168);
            btnOTP.Name = "btnOTP";
            btnOTP.Size = new Size(104, 23);
            btnOTP.TabIndex = 4;
            btnOTP.Text = "Gửi  Mã OTP";
            btnOTP.UseVisualStyleBackColor = true;
            btnOTP.Click += btnOTP_Click;
            // 
            // btnVerifyOTP
            // 
            btnVerifyOTP.AllowDrop = true;
            btnVerifyOTP.BackColor = SystemColors.ScrollBar;
            btnVerifyOTP.Location = new Point(214, 203);
            btnVerifyOTP.Name = "btnVerifyOTP";
            btnVerifyOTP.Size = new Size(81, 25);
            btnVerifyOTP.TabIndex = 5;
            btnVerifyOTP.Text = "Xác Nhận ";
            btnVerifyOTP.UseVisualStyleBackColor = false;
            btnVerifyOTP.Click += btnVerifyOTP_Click;
            // 
            // imageList1
            // 
            imageList1.ColorDepth = ColorDepth.Depth32Bit;
            imageList1.ImageStream = (ImageListStreamer)resources.GetObject("imageList1.ImageStream");
            imageList1.TransparentColor = Color.Transparent;
            imageList1.Images.SetKeyName(0, "logo.png");
            // 
            // button1
            // 
            button1.BackColor = SystemColors.ScrollBar;
            button1.ImageIndex = 0;
            button1.ImageList = imageList1;
            button1.Location = new Point(-5, 0);
            button1.Name = "button1";
            button1.Size = new Size(109, 79);
            button1.TabIndex = 6;
            button1.TabStop = false;
            button1.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 15.75F, FontStyle.Bold, GraphicsUnit.Point, 0);
            label3.ForeColor = Color.Red;
            label3.Location = new Point(224, 49);
            label3.Name = "label3";
            label3.Size = new Size(182, 30);
            label3.TabIndex = 7;
            label3.Text = "Lấy Lại Mật Khẩu";
            // 
            // lblLoginStatus
            // 
            lblLoginStatus.AutoSize = true;
            lblLoginStatus.Location = new Point(205, 256);
            lblLoginStatus.Name = "lblLoginStatus";
            lblLoginStatus.Size = new Size(0, 15);
            lblLoginStatus.TabIndex = 8;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.ScrollBar;
            ClientSize = new Size(574, 280);
            Controls.Add(lblLoginStatus);
            Controls.Add(label3);
            Controls.Add(button1);
            Controls.Add(btnVerifyOTP);
            Controls.Add(btnOTP);
            Controls.Add(txtOTP);
            Controls.Add(textEmail);
            Controls.Add(label2);
            Controls.Add(label1);
            Name = "Form1";
            Text = "Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Label label2;
        private TextBox textEmail;
        private TextBox txtOTP;
        private Button btnOTP;
        private Button btnVerifyOTP;
        private ImageList imageList1;
        private Button button1;
        private Label label3;
        private Label lblLoginStatus;
    }
}
